from typing import List

from fastapi_assistant.bases import BasicDao, ListCustomizeFilterSchema
from api.models import Demo
from fastapi_assistant.libs import get_start_of_the_day


class DemoDao(BasicDao):
    Model = Demo

    def handle_list_customize_filters(self, args_filters: List[ListCustomizeFilterSchema]) -> List:
        filters = []
        for item in args_filters:
            if item.key == 'create_date':
                start_time, end_time = get_start_of_the_day(item.value)
                filters.append(self.Model.create_time.between(start_time, end_time))
        return filters
