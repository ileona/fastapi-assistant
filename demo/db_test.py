import pytest as pytest

from demo.api.comment.mysql import get_database
from demo.api.models import Demo
from fastapi_assistant.core import get_or_create, update_or_create


@pytest.mark.asyncio
async def test_get_one_or_create(db=next(get_database())):
    kwargs = {
        'name': '王二',
    }
    defaults = {
        'turn_on': False
    }
    obj, is_created = await get_or_create(db, Demo, defaults, **kwargs)
    db.commit()
    assert obj is not None
    assert is_created is True

    obj, is_created = await get_or_create(db, Demo, **kwargs)
    db.commit()
    assert obj is not None
    assert is_created is False


@pytest.mark.asyncio
async def test_update_or_create(db=next(get_database())):
    filters = {
        'name': '王二',
    }
    defaults = {
        'turn_on': False,
    }
    obj, is_ = await update_or_create(db, Demo, defaults, **filters)
    db.commit()
    assert obj.name == filters['name']
    assert obj.turn_on == defaults['turn_on']

    filters = {
        'name': '码字',
    }
    defaults = {
        'turn_on': False,
    }
    obj, is_ = await update_or_create(db, Demo, defaults, **filters)
    db.commit()
    assert obj.name == filters['name']
    assert obj.turn_on == defaults['turn_on']
