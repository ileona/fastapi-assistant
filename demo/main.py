import uvicorn

from api.comment.mysql import EnhancedBase, Base, engine
from api.comment.settings_path import builder_settings
from api.controllers import router
from fastapi_assistant.builder_api import builder_fastapi
from fastapi_assistant.log_settings import LOGGING_CONFIG

app = builder_fastapi(
    fastapi_settings=builder_settings.settings.Fastapi.config)

app.include_router(router, prefix='/v1')

Base.metadata.create_all(bind=engine)
EnhancedBase.metadata.create_all(bind=engine)

if __name__ == "__main__":
    service_config = builder_settings.settings.Service
    uvicorn.run(service_config.app, host=service_config.host, port=service_config.port, reload=False,
                log_config=LOGGING_CONFIG)
