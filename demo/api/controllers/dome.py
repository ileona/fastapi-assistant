from datetime import date

from fastapi import APIRouter, Depends, Query
from sqlalchemy.orm import Session

from api.comment import get_database
from demo.api.filters.demo import DemoQueryFilter, test_query
from fastapi_assistant.bases import ListArgsSchema, ListFilterSchema, ListCustomizeFilterSchema, FilterConditionEnum
from demo.api.schemas.demo import CreateDemoSchema
from demo.api.services import demo_service
from fastapi_assistant.libs import Pagination

router = APIRouter(prefix="/demo", tags=["demo"])


@router.post("", status_code=201, name="create")
async def create(body: CreateDemoSchema, db: Session = Depends(get_database)):
    user = await demo_service.create(db, body)
    return user.to_dict()


@router.get("", status_code=200, name="list")
async def get_list(
    name: str = Query(default=None, title="name"),
    turn_on: bool = Query(default=None, title="开启"),
    ids: str = Query(default=None, title="ids"),
    create_date: date = Query(default=None, title="创建日期"),
    pagination=Depends(Pagination),
    db: Session = Depends(get_database),
):
    args = ListArgsSchema(page=pagination.page, size=pagination.size)
    if name is not None:
        args.filters.append(ListFilterSchema(key="name", condition=FilterConditionEnum.like, value=name))
    if turn_on is not None:
        condition = FilterConditionEnum.true if turn_on else FilterConditionEnum.false
        args.filters.append(ListFilterSchema(key="turn_on", condition=condition))
    if ids is not None:
        args.filters.append(
            ListFilterSchema(key="id", condition=FilterConditionEnum.in_, value=[int(i) for i in ids.split(",")])
        )
    if create_date is not None:
        args.customize_filters.append(ListCustomizeFilterSchema(key="create_date", value=create_date))
    # args.filters.append(ListFilterSchema(key='deleted', condition=False))
    # args.filters.append(ListFilterSchema(key='id', condition=FilterConditionEnum.between, value=[1, 3]))
    return await demo_service.list(db, args)


@router.get("/{pk}", status_code=200, name="get")
async def get(pk: int, db: Session = Depends(get_database)):
    user = await demo_service.get(db, pk)
    return user.to_dict()


@router.delete("/{pk}", status_code=200, name="delete")
async def delete(pk: int, db: Session = Depends(get_database)):
    await demo_service.delete(db, pk)
    return


@router.get("/test/filter", status_code=200, name="delete")
async def test_filter(
    query_filters: DemoQueryFilter = Depends(test_query),
    pagination=Depends(Pagination),
    db: Session = Depends(get_database),
):
    # query = query_filters.apply_filters(db)
    # return query.all()
    filters = query_filters.get_filters()
    return await demo_service.dao.list_by_filters(db, filters)