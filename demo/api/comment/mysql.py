from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL
from sqlalchemy.pool import NullPool

from api.comment.settings_path import builder_settings
from fastapi_assistant.bases import BasicModel, EnhancedModel
from fastapi_assistant.builder_db_engine import BuilderDbEngine

# -------- 使用一 使用内置sqlit3, 或自己配路径 -----------
# builder_db_engine = BuilderDbEngine(poolclass=NullPool)
builder_db_engine = BuilderDbEngine(settings_=builder_settings.settings, echo=True)
engine = builder_db_engine.engine

# -------- 使用二 使用mysql配置 -----------
# builder_db_engine = BuilderDbEngine(settings_=builder_settings.settings)
# engine = builder_db_engine.engine

# -------- 使用三 mysql 字典创建 -----------
# mysql_config = {
#     'username': 'root',
#     'password': '123456',
#     'host': '127.0.0.1',
#     'port': 3306,
#     'database': 'project',
# }
# builder_db_engine = BuilderDbEngine(db_config=mysql_config)
# engine = builder_db_engine.engine

# -------- 使用四 自己构建engine -----------
# my_url = URL.create(
#     drivername="mysql+pymysql",
#     username='root',
#     password='123456',
#     host='127.0.0.1',
#     port=3306,
#     database='project',
# )
# engine = create_engine(my_url, poolclass=NullPool)
# builder_db_engine = BuilderDbEngine(engine)


Base = builder_db_engine.get_base(cls=BasicModel)
EnhancedBase = builder_db_engine.get_base(cls=EnhancedModel)
get_database = builder_db_engine.get_database
