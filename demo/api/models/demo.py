from sqlalchemy import Column, String, Boolean

from api.comment.mysql import Base, EnhancedBase


class Demo(Base):
    """demo"""
    __tablename__ = 'demo'
    __table_args__ = ({'comment': 'demo', 'extend_existing': True})
    name = Column(String(255), nullable=True, default='', comment='名字')
    turn_on = Column(Boolean, default=False, comment='开启')
    deleted = Column(Boolean, default=False, comment='是否删除')


class EnhancedDemo(EnhancedBase):
    """demo"""
    __tablename__ = 'enhanced_demo'
    __table_args__ = ({'comment': 'enhanced_demo', 'extend_existing': True})

    name = Column(String(255), nullable=True, default='', comment='名字')
