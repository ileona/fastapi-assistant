from typing import Any, Coroutine
from sqlalchemy.orm import Session

from api.schemas.demo import CreateDemoSchema
from fastapi_assistant.bases import BasicService, RespListSchema
from api.daos import DemoDao
from api.models import Demo
from fastapi_assistant.bases.schema import ListArgsSchema, RespListSchema


class DemoService(BasicService):

    def __init__(self, operator_id=0):
        super(DemoService, self).__init__(operator_id)
        self.Model = Demo
        self.dao = DemoDao(operator_id)
        self.dao.Model = Demo

    async def list(self, db: Session, args: ListArgsSchema) -> RespListSchema:
        return await super(DemoService, self).list(db, args)

    async def create(self, db: Session, schema: CreateDemoSchema) -> Demo:
        obj = await self.dao.dict_create(db, dict(schema))
        # obj, _ = await self.dao.get_or_create(db, name=schema.name, defaults={
        #     self.Model.turn_on.key: schema.turn_on
        # })
        return obj


demo_service = DemoService()
