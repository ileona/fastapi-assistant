from typing import Optional
from fastapi import Query
from pydantic import Field
from demo.api.models.demo import Demo
from fastapi_assistant.bases.enums import FilterConditionEnum
from fastapi_assistant.bases.filter import FilterSet, QueryField


class DemoQueryFilter(FilterSet):
    """
    Demo filter
    """

    class Meta:
        model = Demo
        fields = ["name", "turn_on", "ids"]

    name: str = Field(
        default_factory=str,
        description="Demo name",
        query=QueryField(field_name="name", lookup_expr=FilterConditionEnum.like),
    )
    turn_on: Optional[bool] = Field(
        default_factory=bool,
        description="Demo turn on",
        query=QueryField(field_name="turn_on"),
    )


def test_query(
    name: str = Query(default="", title="name"),
    turn_on: bool = Query(default=None, title="开启"),
):
    return DemoQueryFilter(name=name, turn_on=turn_on)
