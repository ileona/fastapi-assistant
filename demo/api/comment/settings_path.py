import os
import sys
from pathlib import Path

# ----- 使用外部包
rootPath = Path(__file__).resolve().parent.parent.parent.parent
sys.path.append(os.path.abspath(os.path.join(rootPath)))

from fastapi_assistant import BuilderSettings

base_dir = Path(__file__).resolve().parent.parent.parent
default_setting = 'settings.ini'
builder_settings = BuilderSettings(base_dir, default_setting)
