from pydantic import BaseModel, Field


class CreateDemoSchema(BaseModel):
    name: str = Field(..., description='名称')
    turn_on: bool = Field(default=False, description='开启')
